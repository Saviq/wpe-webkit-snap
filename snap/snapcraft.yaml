name: wpe-webkit-mir-kiosk
base: core20
adopt-info: wpe-webkit
summary: WPE Webkit in kiosk mode. Plug-n-play browsing for embedded devices.
description: |
  WPE Webkit is an official port of the WebKit project.

  Quoting the WPE website [1]: “If you need a fast and lightweight web runtime for embedded devices that supports most current web standards, has hardware acceleration wherever it is advantageous, and has a strong focus on multimedia applications, WPE WebKit is a great choice.”

  This snap packages WPE, backend libraries for the reference FDO graphics stack, and the cog web app container [2] in a ready-to-use kiosk mode. Requires the `ubuntu-frame` snap (formerly `mir-kiosk`) on Ubuntu Core, or a compatible Wayland compositor on desktop systems.

  See the README [3] for limitations and options.

  On Core systems, the browser is running as a service, and will be automatically restarted after `ubuntu-frame` / `mir-kiosk` refreshes.
  On desktop systems, the application `wpe-webkit-mir-kiosk.cog` starts the launcher in windowed mode, suitable for testing.

  1: https://wpewebkit.org
  2: https://github.com/Igalia/cog
  3: https://gitlab.com/glancr/wpe-webkit-snap/-/blob/main/README.md

confinement: strict
compression: lzo

layout:
  # Relies on cmake install prefix $SNAP/usr, /libexec is not allowed
  /usr/libexec/wpe-webkit-1.0:
    bind: $SNAP/usr/libexec/wpe-webkit-1.0
  /usr/lib/$SNAPCRAFT_ARCH_TRIPLET/cog:
    bind: $SNAP/usr/lib/$SNAPCRAFT_ARCH_TRIPLET/cog
  /usr/lib/$SNAPCRAFT_ARCH_TRIPLET/wpe-webkit-1.0:
    bind: $SNAP/usr/lib/$SNAPCRAFT_ARCH_TRIPLET/wpe-webkit-1.0
  /usr/share/X11:
    bind: $SNAP/usr/share/X11
  /usr/share/zoneinfo-icu:
    bind: $SNAP/usr/share/zoneinfo-icu
  /usr/local/share/fonts: # wpe-webkit: Search ALL the fonts!
    bind: $SNAP/usr/share/fonts

  ### MIR SNIPPET
  # https://github.com/MirServer/iot-example-graphical-snap/blob/1f69ee310d009f0ea9cbb8654296abc948bafb15/snap/snapcraft.yaml#L54
  # These paths (/usr/share/libdrm and /usr/share/drirc.d) are hardcoded in mesa.
  /usr/share/libdrm:  # Needed by mesa-core20 on AMD GPUs
    bind: $SNAP/graphics/libdrm
  /usr/share/drirc.d:  # Used by mesa-core20 for app specific workarounds
    bind: $SNAP/graphics/drirc.d
  # Other, generally useful paths
  /usr/share/fonts:
    bind: $SNAP/usr/share/fonts
  /usr/share/icons:
    bind: $SNAP/usr/share/icons
  /usr/share/sounds:
    bind: $SNAP/usr/share/sounds
  /etc/fonts:
    bind: $SNAP/etc/fonts
  ### END MIR SNIPPET

environment:
  LC_ALL: C.UTF-8
  GST_PLUGIN_PATH: ${SNAP}/usr/lib/${SNAPCRAFT_ARCH_TRIPLET}/gstreamer-1.0
  GST_PLUGIN_SYSTEM_PATH: ${SNAP}/usr/lib/${SNAPCRAFT_ARCH_TRIPLET}/gstreamer-1.0
  GST_PLUGIN_SCANNER: ${SNAP}/usr/lib/${SNAPCRAFT_ARCH_TRIPLET}/gstreamer1.0/gstreamer-1.0/gst-plugin-scanner
  FONTCONFIG_PATH: ${SNAP}/etc/fonts/conf.d
  FONTCONFIG_FILE: ${SNAP}/etc/fonts/fonts.conf

  ### MIR SNIPPET
  # https://github.com/MirServer/iot-example-graphical-snap/blob/1f69ee310d009f0ea9cbb8654296abc948bafb15/snap/snapcraft.yaml#L41
  # LD_LIBRARY_PATH: $SNAP/graphics/lib # ORIGINAL
  LD_LIBRARY_PATH: ${SNAP}/graphics/lib:${SNAP}/usr/lib/${SNAPCRAFT_ARCH_TRIPLET}/pulseaudio
  LIBGL_DRIVERS_PATH: $SNAP/graphics/dri
  LIBVA_DRIVERS_PATH: $SNAP/graphics/dri
  # __EGL_VENDOR_LIBRARY_DIRS: $SNAP/graphics/glvnd/egl_vendor.d # ORIGINAL
  __EGL_VENDOR_LIBRARY_DIRS: $SNAP/graphics/glvnd/egl_vendor.d:$SNAP/etc/glvnd/egl_vendor.d:$SNAP/usr/share/glvnd/egl_vendor.d
  # Other, generally useful environment settings...
  # XDG config
  XDG_CACHE_HOME: $SNAP_USER_COMMON/.cache # overridden in apps
  XDG_CONFIG_HOME: $SNAP_USER_DATA/.config # overridden in apps
  XDG_CONFIG_DIRS: $SNAP/etc/xdg # overridden in apps
  # XKB config
  XKB_CONFIG_ROOT: $SNAP/usr/share/X11/xkb
  ### END MIR SNIPPET

plugs:
  # Auto-connected
  wayland:
  opengl: # required for libEGL to work
  network:
  network-bind: # Remote inspector
  upower-observe:
  # Manually connected, show up as AppArmor denials but
  # basic browsing seems to work fine without
  avahi-observe: # zeroconf name resolution
  audio-playback:
  hardware-observe: # required for /sys/firmware/acpi/pm_profile and /sys/class/dmi/id/chassis_type access
  network-manager:
  hostname-control:
  process-control:
  system-observe: # required for /proc/zoneinfo access
  ### MIR SNIPPET
  # https://github.com/MirServer/iot-example-graphical-snap/blob/1f69ee310d009f0ea9cbb8654296abc948bafb15/snap/snapcraft.yaml#L35
  graphics-core20:
    interface: content
    target: $SNAP/graphics
    default-provider: mesa-core20
  ### END MIR SNIPPET

slots:
  dbus-cogctl:
    interface: dbus
    bus: system
    name: com.igalia.Cog

apps:
  cog:
    command-chain: &_command-chain
      - bin/wayland-launch
      - bin/set-arch-triplet
      - bin/gio-updater
    command: &_command bin/launch-wpe
    environment:
      # XDG config, see https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
      XDG_DATA_HOME: ${SNAP_USER_DATA}/usr/share
      XDG_CONFIG_HOME: ${SNAP_USER_DATA}/.config
      XDG_DATA_DIRS: ${SNAP_USER_COMMON}/usr/share:${SNAP}/usr/share:${XDG_DATA_DIRS}
      XDG_CONFIG_DIRS: ${SNAP}/etc/xdg:$XDG_CONFIG_DIRS
      XDG_CACHE_HOME: ${SNAP_USER_COMMON}/.cache
      # Writable cache for media files. See https://github.com/WebKit/WebKit/blob/b9fedd48387fde72b00ac63364bc447a59749ab8/Source/WebCore/platform/graphics/gstreamer/MediaPlayerPrivateGStreamer.cpp#L2176 and https://matrix.to/#/!CTTPZAhLfjGrOmKckM:matrix.org/$wtBihALd1XnUQUfA6bqJ-q8iD4sghED8LtxlhseH4bg?via=igalia.com&via=matrix.org&via=h6t.eu
      # The default is /var/tmp which should be persisted across reboots. However, since this is a Kiosk browser, we cannot assume that someone will clean that up regularly.
      WPE_SHELL_MEDIA_DISK_CACHE_PATH: ${XDG_CACHE_HOME}

  daemon:
    command-chain: *_command-chain
    command: *_command
    daemon: simple
    restart-condition: always
    restart-delay: 3s
    slots: [dbus-cogctl]
    environment:
      # XDG config, see https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
      XDG_DATA_HOME: ${SNAP_DATA}/usr/share
      XDG_CONFIG_HOME: ${SNAP_DATA}/.config
      XDG_DATA_DIRS: ${SNAP_COMMON}/usr/share:${SNAP}/usr/share:$XDG_DATA_DIRS
      XDG_CONFIG_DIRS: ${SNAP}/etc/xdg:${XDG_CONFIG_DIRS}
      XDG_CACHE_HOME: ${SNAP_COMMON}/.cache
      # Writable cache for media files. See https://github.com/WebKit/WebKit/blob/b9fedd48387fde72b00ac63364bc447a59749ab8/Source/WebCore/platform/graphics/gstreamer/MediaPlayerPrivateGStreamer.cpp#L2176 and https://matrix.to/#/!CTTPZAhLfjGrOmKckM:matrix.org/$wtBihALd1XnUQUfA6bqJ-q8iD4sghED8LtxlhseH4bg?via=igalia.com&via=matrix.org&via=h6t.eu
      # The default is /var/tmp which should be persisted across reboots. However, since this is a Kiosk browser, we cannot assume that someone will clean that up regularly.
      WPE_SHELL_MEDIA_DISK_CACHE_PATH: ${XDG_CACHE_HOME}

  restart-watcher:
    command: bin/watcher
    daemon: simple
    restart-condition: always

  list-websettings:
    command: usr/bin/cog --help-websettings

  clear-cache:
    command: bin/clear-cache 

parts:
  libwpe:
    source: https://wpewebkit.org/releases/libwpe-1.14.1.tar.xz
    plugin: meson
    # https://github.com/WebPlatformForEmbedded/libwpe/blob/1.14.0/.github/workflows/build.yml#39
    meson-version: "0.55"
    meson-parameters:
      - --buildtype=release
      - --prefix=/usr
    build-packages:
      - build-essential
      - libegl1-mesa-dev
      - libxkbcommon-dev
    stage-packages:
      - libxkbcommon0

  wpebackend-fdo:
    after: [libwpe]
    source: https://wpewebkit.org/releases/wpebackend-fdo-1.14.0.tar.xz
    plugin: meson
    # https://github.com/Igalia/WPEBackend-fdo/blob/1.14.0/.github/workflows/ci.yml#L33
    meson-version: "0.55"
    meson-parameters:
      - --buildtype=release
      - --prefix=/usr
    build-packages:
      - build-essential
      - ccache
      - libegl1
      - libegl-mesa0
      - libegl1-mesa-dev
      - libwayland-egl1
      - libglib2.0-dev
      - libepoxy-dev
      - libxkbcommon-dev
      - libxml2-dev
      - libxslt1-dev
      - libyaml-dev
    stage-packages:
      - libegl1-mesa
      - libepoxy0
      - libwayland-egl1
      - libwayland-client0
      - libwayland-server0
      - libxkbcommon0

  wpe-webkit:
    after: [wpebackend-fdo]
    source: https://wpewebkit.org/releases/wpewebkit-2.38.5.tar.xz
    plugin: cmake
    cmake-generator: Ninja
    cmake-parameters:
      - -DPORT=WPE
      - -DCMAKE_BUILD_TYPE=Release
      - -DCMAKE_INSTALL_PREFIX:PATH=/usr
      # Disable documentation builds.
      - -DENABLE_DOCUMENTATION=OFF
      - -DENABLE_INTROSPECTION=OFF
      # We do not need a sandbox when running fully confined.
      - -DENABLE_BUBBLEWRAP_SANDBOX=OFF
      # Ubuntu 20.04 does not provide libsoup 3.
      - -DUSE_SOUP2=ON
      # Ensure wpe-webkit finds libWPEBackend-fdo-1.0.so.1
      - -DCMAKE_BUILD_RPATH="${SNAPCRAFT_STAGE}/usr/lib/${SNAPCRAFT_ARCH_TRIPLET}"
    build-packages:
      - gperf
      - ninja-build
      - libcairo2-dev
      - liblcms2-dev
      - libepoxy-dev
      - libgbm-dev
      - libgcrypt20-dev
      - libgnutls28-dev
      - libgstreamer-plugins-base1.0-dev
      - libgstreamer-plugins-bad1.0-dev
      - libharfbuzz-dev
      - libicu-dev
      - libjpeg8-dev
      - libopenjp2-7-dev
      - libsoup2.4-dev
      - libsqlite3-dev
      - libsystemd-dev
      - libtasn1-6-dev
      - libwebp-dev
      - libwoff-dev
      - libxml2-dev
      - libxslt1-dev
      - pkg-config
      - python3
      - ruby-dev
      - wayland-protocols
      # TODO: Are these still required without the minibrowser?
      - zlib1g-dev
      - libatk1.0-dev
      - libatk-bridge2.0-dev
    stage-packages:
      - freeglut3
      - libatk1.0-0 # TODO: Required at runtime?
      - libatk-bridge2.0-0 # TODO: Required at runtime?
      - libgpm2
      - libwayland-client0
      - libwayland-server0
      - libcairo2
      - libdrm2
      - libepoxy0
      - libgbm1
      - libgl1
      - libgl1-mesa-dri
      - libgl1-mesa-glx
      - libglu1-mesa
      - libglx0
      - libgcrypt20
      - libgstreamer-gl1.0-0
      - libharfbuzz-icu0
      - libicu66
      - libjpeg8
      - libopenjp2-7
      - liborc-0.4-0
      - libsoup2.4-1
      - libsqlite3-0
      - libsystemd0
      - libtasn1-6
      - libwebp6
      - libwebpdemux2
      - libwoff1
      - libxml2
      - libxml2-utils
      - libxslt1.1
      - python3
      - ruby
      - v4l-utils
      - zlib1g
      # from https://github.com/Igalia/meta-webkit/blob/e0d5fb3c66b910984464f1f22567a0cd618331ae/recipes-browser/wpewebkit/wpewebkit.inc#L87
      - gstreamer1.0-gl
      - gstreamer1.0-libav
      - gstreamer1.0-plugins-base
      - gstreamer1.0-plugins-base-apps
      # FIXME: Pull in only packages referenced in Yocto image to avoid bloat
      - gstreamer1.0-plugins-good
      - gstreamer1.0-plugins-bad
      - to amd64:
          - libatomic1
      - to arm64:
          - libatomic1
    override-build: |
      WPE_VERSION=$(grep -E 'SET_PROJECT_VERSION' "$SNAPCRAFT_PART_SRC/Source/cmake/OptionsWPE.cmake" | tr -d '[:alpha:]_()' | tr '[:blank:]' '.')

      snapcraftctl set-version "$WPE_VERSION"

      # Even-numbered minor versions indicate stable releases. See https://wpewebkit.org/release/schedule/
      WPE_MINOR_VERSION=$(echo "$WPE_VERSION" | cut -d'.' -f2)
      if [ $(( WPE_MINOR_VERSION % 2)) -eq 0 ]; then
        snapcraftctl set-grade stable
      else
        snapcraftctl set-grade devel
      fi

      snapcraftctl build
    override-prime: |
      snapcraftctl prime

      # Updates the snap's MIME database, e.g. for PDF.js resources to load properly.
      usr/bin/update-mime-database usr/share/mime

  # See Source/cmake/WebKitFeatures.cmake for build-time options.
  # See Source/cmake/OptionsWPE.cmake for overrides applied by WPE.
  # See Source/cmake/OptionsWPE.cmake for required packages.
  cog:
    after: [wpe-webkit]
    source: https://wpewebkit.org/releases/cog-0.16.1.tar.xz
    plugin: meson
    # https://github.com/Igalia/cog/blob/134a13b4f39ac8e8d31a8a514a7041bdb4bc17a2/.github/workflows/ci-native.yml#L35
    meson-version: "0.55"
    meson-parameters:
      # see https://github.com/Igalia/cog/blob/0.16.1/meson_options.txt
      - --buildtype=release
      - --prefix=/usr
      - -Dcog_dbus_control=system
      - -Dplatforms=wayland
      - -Dmanpages=false
      # Ubuntu 20.04 does not provide libsoup 3.
      - -Dsoup2=auto
    build-environment:
      - LD_LIBRARY_PATH: $LD_LIBRARY_PATH:$SNAPCRAFT_STAGE/usr/lib/$SNAPCRAFT_ARCH_TRIPLET
    build-packages:
      - ninja-build
      - libcairo2-dev
      - libsoup2.4-dev
      - libxkbcommon-dev
      - libwayland-dev
      - libegl1-mesa-dev
      - libwayland-bin
      - wayland-protocols
    stage-packages:
      # Runtime requirements for cog browser
      - glib-networking # required for SSL/TLS support
      - glib-networking-common
      - glib-networking-services
      - libgles2
      - libslang2 # dep for gstreamer ASCII art modules ¯\_(ツ)_/¯
      - libgpm2 # mouse
      - libgdk-pixbuf2.0-0
      # TODO: Fix zeroconf/Bonjour name resolution, these don't seem to suffice
      - libavahi-core7
      - libavahi-glib1
      - libavahi-common3
      - libavahi-client3
      - libwayland-cursor0
      # dependencies listed for cog 0.4.0 debian package:
      - libxkbcommon0
      - libegl1
      - libglib2.0-0
      - libsoup2.4-1
      - libwayland-client0
      - libwayland-egl1
      - freeglut3
      - libglu1-mesa
      - shared-mime-info
    override-prime: |
      snapcraftctl prime
      glib-compile-schemas usr/share/glib-2.0/schemas/
      # see https://stackoverflow.com/questions/28953925/glib-gio-error-no-gsettings-schemas-are-installed-on-the-system

  launch-wpe:
    plugin: dump
    source: src/launcher
    organize:
      $SNAPCRAFT_PART_SRC/*: bin/

  restart-watcher:
    plugin: dump
    source: src/watcher
    stage-packages:
      - inotify-tools
    organize:
      watcher: bin/

  ### MIR SNIPPET
  # https://github.com/MirServer/iot-example-graphical-snap/blob/1f69ee310d009f0ea9cbb8654296abc948bafb15/snap/snapcraft.yaml#L75
  wayland-launch:
    plugin: dump
    source: wayland-launch
    override-build: |
      # The plugs needed to run Wayland. (wayland-launch checks them, setup.sh connects them)
      # You may add further plugs here if you want these options
      PLUGS="opengl wayland graphics-core20 audio-playback"
      sed --in-place "s/%PLUGS%/$PLUGS/g" $SNAPCRAFT_PART_BUILD/bin/wayland-launch
      sed --in-place "s/%PLUGS%/$PLUGS/g" $SNAPCRAFT_PART_BUILD/bin/setup.sh
      snapcraftctl build
    stage-packages:
      - inotify-tools
  ### END MIR SNIPPET

  ### MIR SNIPPET
  # https://github.com/MirServer/iot-example-graphical-snap/blob/1f69ee310d009f0ea9cbb8654296abc948bafb15/snap/snapcraft.yaml#L89
  cleanup:
    after:
      - libwpe
      - wpebackend-fdo
      - wpe-webkit
      - cog
      - wayland-launch
    plugin: nil
    build-snaps: [ mesa-core20 ]
    override-prime: |
      set -eux
      cd /snap/mesa-core20/current/egl/lib
      find . -type f,l -exec rm -f $SNAPCRAFT_PRIME/usr/lib/${SNAPCRAFT_ARCH_TRIPLET}/{} \;
      rm -fr "$SNAPCRAFT_PRIME/usr/lib/${SNAPCRAFT_ARCH_TRIPLET}/dri"
      for CRUFT in bug drirc.d glvnd libdrm lintian man; do
        rm -rf "$SNAPCRAFT_PRIME/usr/share/$CRUFT"
      done
  ### END MIR SNIPPET
